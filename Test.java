static void TestShellSortComplexitivity {
    static void test() {
        final int maxSize = 4194304; // 4 Mb
        int longArr[] = getRandArray(maxSize);

        System.out.println("Complexitivity");
        System.out.println("Size             Time");

        shellSortAndPrintTime(longArr, maxSize);
        shellSortAndPrintTime(longArr, maxSize/2);
        shellSortAndPrintTime(longArr, maxSize/4);
        shellSortAndPrintTime(longArr, maxSize/8);
        shellSortAndPrintTime(longArr, maxSize/16);
        shellSortAndPrintTime(longArr, maxSize/32);
        shellSortAndPrintTime(longArr, maxSize/64);
    }

    private void shellSortAndPrintTime(int[] arr, int size) {
        long timeBegin = System.nanoTime();
        shellSort(Arrays.copyOfRange(arr, 0, size));
        long timeEnd = System.nanoTime();
        System.out.println(size + "        " +  ((timeEnd - timeBegin)*0.000000001));
    }

    private int[] getRandArray(int maxSize) {
        int a[] = new int[maxSize];
        Random r = new Random();
        for (int i = 0; i < a.length; i++) {
            a[i] = r.nextInt();
        }
        return a;
    }
}