import java.util.*;
import java.lang.*;
import java.io.*;


/*
Shell sort algorithm in java
Sample from wiki: https://en.wikipedia.org/wiki/Shellsort#Pseudocode
See in Bitbucket repo: 
*/
class ShellSort
{
	    static void shellSort(int[] a) {
        final int GAPS[] = {701, 301, 132, 57, 23, 10, 4, 1};
        int n = a.length;

        for (int gap : GAPS) {
            // Do a gapped insertion sort for this gap size.
            // The first gap elements a[0..gap-1] are already in gapped order
            // keep adding one more element until the entire array is gap sorted
            for (int i = gap; i < n; i += 1) {
                // add a[i] to the elements that have been gap sorted
                // save a[i] in temp and make a hole at position i
                int temp = a[i];
                // shift earlier gap-sorted elements up until the correct location for a[i] is found
                int j;
                for (j = i; j >= gap && a[j - gap] > temp; j -= gap) {
                    a[j] = a[j - gap];
                }
                // put temp (the original a[i]) in its correct location
                a[j] = temp;
            }
        }
    }
	
	public static void main (String[] args) throws java.lang.Exception
	{
		// Verifying...
		int arr[] = {62, 83, 18, 53, 07, 17, 95, 86, 47, 69, 25, 28};
        System.out.println(Arrays.toString(arr));
        shellSort(arr);
        System.out.println(Arrays.toString(arr));
	}
}