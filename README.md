# SELFEDUCATION #

Sample of [ShellSort](https://en.wikipedia.org/wiki/Shellsort#Description) algorithm in java.

You can verify results in online compiler: http://ideone.com/fc6WdF

Timing results

#Complexitivity

Size           Time

4194304        40.168

2097152        07.399

1048576        01.141

524288         00.174

262144         00.053

131072         00.020

65536          00.007